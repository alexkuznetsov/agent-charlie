# == Class: idea
#
# Sets up intellij
#

class idea () {
    case $::operatingsystem {
        Ubuntu: {
            $clazz = 'idea::intellij::ubuntu'
        }
        default: {
            fail("Your operating system '${::operatingsystem}' is not supported for IntelliJ IDEA")
        }
    }

    anchor { 'idea::intellij::begin': }

    class {$clazz:
        require => Anchor['idea::intellij::begin'],
        before  => Anchor['idea::intellij::end'],
    }

    anchor { 'idea::intellij::end': }
}
