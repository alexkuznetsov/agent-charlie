# == Class: jdk
#
# Sets up the JDK and chooses the appropriate class to install the JDK for
#

class jdk {
    anchor { 'jdk::begin': }

    case $::operatingsystem {
        ubuntu: {
            class{ 'jdk::ubuntu':
                require => Anchor['jdk::begin'],
                before  => Anchor['jdk::end'],
            }
        }
        Darwin: {
            class { 'jdk::darwin':
                require => Anchor['jdk::begin'],
                before  => Anchor['jdk::end'],
            }
        }
        default: {
            fail('Unsupported operating system for the JDK')
        }
    }

    anchor { 'jdk::end': }
}