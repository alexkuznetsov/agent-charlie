class hg {
    $tmp = $::config_tmp_directory
    $charlie_dir = $::config_charlie_home

    $user = $::config_user
    $group = $::config_group

    case $::operatingsystem {
        ubuntu: {
            package { 'mercurial':
                ensure => installed
            }
        }
        Darwin: {
            exec {'install_mercurial':
                command => "${charlie_dir}/bin/brew-bottle mercurial",
                creates => '/usr/local/Cellar/mercurial',
            }
        }
        default: {
            fail("Unsupported operating: ${::operatingsystem}")
        }
    }
}
