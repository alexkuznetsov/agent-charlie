require 'puppet_helper/stash_singleton'
require 'puppet_helper/drop_permissions'

module Puppet::Parser::Functions
    newfunction(:stash_username, :type => :rvalue, :arity => 0) do |args|
        begin
            username = Facter.config_stash_username || ""
        rescue
        end
        
        if username == nil || username.length == 0
            DropPermissions::dropRoot do
                singleton = StashSingleton.instance
                username = singleton.username
            end
        end

        username
    end
end
