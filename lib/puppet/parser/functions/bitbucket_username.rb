require 'puppet_helper/bitbucket_singleton'
require 'puppet_helper/drop_permissions'

module Puppet::Parser::Functions
    newfunction(:bitbucket_username, :type => :rvalue, :arity => 0) do |args|
        begin
            username = Facter.config_bitbucket_username || ""
        rescue
        end
        
        if username == nil || username.length == 0
            DropPermissions::dropRoot do
                singleton = BitbucketSingleton.instance
                username = singleton.username
            end
        end

        username
    end
end
