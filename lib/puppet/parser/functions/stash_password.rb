require 'puppet_helper/stash_singleton'
require 'puppet_helper/drop_permissions'

module Puppet::Parser::Functions
    newfunction(:stash_password, :type => :rvalue, :arity => 0) do |args|
        begin
            password = Facter.config_stash_password || ""
        rescue
        end

        if password == nil || password.length == 0
            DropPermissions::dropRoot do
                singleton = StashSingleton.instance
                password = singleton.password
            end
        end

        password
    end
end
