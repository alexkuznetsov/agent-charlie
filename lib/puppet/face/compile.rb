require 'puppet/face'

require 'puppet_helper/drop_permissions'
require 'fileutils'

Puppet::Face.define(:compile, '1.0.0') do
    summary 'Compiles multiple manifests'

    def apply_catalog(catalog)
        configurer = Puppet::Configurer.new
        configurer.run(:catalog => catalog, :pluginsync => false)
    end

    def create_catalogs(names)
        charlie = AgentCharlie::Charlie.instance
        task_progress = charlie.current_action.get_progress
        catalogs = []

        task_progress.current = 0
        task_progress.max = names.length
        task_progress.message = "Compiling catalogs. Please don't go AFK!!!"

        names.each_with_index do |manifest_key, index|
            manifest_path = File.join(AgentCharlie.charlie_root, 'manifests', manifest_key + '.pp');
            metadata_path = File.join(AgentCharlie.charlie_home, "metadata", manifest_key)

            if ! File.exists?(metadata_path)
                raise "Could not find file #{manifest}" unless ::File.exist?(manifest_path)

                configured_environment = Puppet.lookup(:current_environment)
                apply_environment = configured_environment.override_with(:manifest => manifest_path)

                Puppet.override({:current_environment => apply_environment}, "For puppet compile") do
                  begin
                      unless Puppet[:node_name_fact].empty?
                        # Collect our facts.
                          unless (facts = Puppet::Node::Facts.indirection.find(Puppet[:node_name_value]))
                              raise "Could not find facts for #{Puppet[:node_name_value]}"
                          end

                          Puppet[:node_name_value] = facts.values[Puppet[:node_name_fact]]
                          facts.name = Puppet[:node_name_value]
                      end

                      # Find our Node
                      unless (node = Puppet::Node.indirection.find(Puppet[:node_name_value]))
                          raise "Could not find node #{Puppet[:node_name_value]}"
                      end

                      # Merge in the facts.
                      node.merge(facts.values) if facts
                  rescue => detail
                      Puppet.log_exception(detail)
                      raise detail
                  end

                  # Compile our catalog
                  task_progress.message = "Compiling '#{manifest_key}'..."
                  starttime = Time.now
                  catalog = Puppet::Resource::Catalog.indirection.find(node.name, :use_node => node)

                  # Translate it to a RAL catalog
                  catalog = catalog.to_ral
                  catalog.finalize
                  catalog.retrieval_duration = Time.now - starttime

                  catalogs << catalog
                  task_progress.increment("Compiled catalog for '#{manifest_key}'")
                end
            else
                catalogs << nil
                task_progress.increment("Already installed: '#{manifest_key}'")
            end
        end

        return catalogs
    end

    def install_catalogs(names, catalogs)
        charlie = AgentCharlie::Charlie.instance
        task_progress = charlie.current_action.get_progress

        cname_pair = names.zip(catalogs).find_all do |name,catalog|
            name != nil && catalog != nil
        end

        task_progress.current = 0
        task_progress.max = cname_pair.length
        task_progress.message = 'Installing stuff for you. Feel free to go get a coffee.'

        cname_pair.each do |name, catalog|
            metadata_path = File.join(AgentCharlie.charlie_home, 'metadata', name)

            if ! File.exists?(metadata_path)
                task_progress.message = "Installing '#{name}'..."
                result = apply_catalog(catalog)
                task_progress.increment("Installed '#{name}'")
                if result == 0 || result == 2
                    DropPermissions::dropRoot do
                        FileUtils.mkdir_p(File.dirname(metadata_path))
                        File.open(metadata_path, 'w') { |file|
                            file.write(catalog.retrieval_duration) # Something random
                        }
                    end
                else
                    raise "There was a problem applying the manifest"
                end
            else
                task_progress.increment("#{name} already installed")
            end
        end
    end

    action(:compile) do
        default

        when_invoked do |*options|
            names = options[0, options.length-1]

            begin
                catalogs = create_catalogs(names)
                install_catalogs(names, catalogs)
            rescue => detail
                Puppet.log_exception(detail)
                raise detail
            end

            # Something about pson_data_hash here if we don't exit or return...
            next 0
        end
    end
end
