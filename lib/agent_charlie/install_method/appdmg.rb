class AgentCharlie::InstallMethod::AppDmg < AgentCharlie::InstallMethod::Base

    def self.description
        'Installed into OS X /Applications'
    end

    def parse(data)
        # Nothing to do here. Move along.
    end

    def description
        ''
    end

end

AgentCharlie::InstallMethod.new_method(AgentCharlie::InstallMethod::AppDmg, 'appdmg')