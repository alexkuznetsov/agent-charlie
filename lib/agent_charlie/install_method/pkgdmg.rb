class AgentCharlie::InstallMethod::PkgDmg < AgentCharlie::InstallMethod::Base

    def self.description
        "Installed with a 'pkg' installer"
    end

    def parse(data)
        # Nothing to do here
    end

    def description
        return ""
    end

end

AgentCharlie::InstallMethod.new_method(AgentCharlie::InstallMethod::PkgDmg, "pkgdmg")