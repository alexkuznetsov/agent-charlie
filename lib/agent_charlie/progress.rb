# This class indicates the progress of any running action
class AgentCharlie::Progress

    # Ruby does not support enums so I will do this instead
    @stages = [
        :WAITING, # Waiting in the event queue to be run
        :RUNNING, # Running
        :FINISHED, # Finished
        :FAILED, # A problem occurred and the task could not finish
    ]

    def initialize(max = nil, &block)
        @stage = :WAITING
        @current = 0
        @max = max
        @block = block

        @message = nil

        @block.call(self)
    end

    def stage
        @stage
    end

    def percentage
        return '  ?.?' if @max == nil
        percentage = @max > 0 ? (@current.to_f/@max.to_f)*100.0 : 0

        '%5.1f' % percentage
    end

    def current
        @current
    end

    def max
        @max
    end

    def increment(message)
        raise 'Cannot change progress not currently running' unless @stage == :RUNNING
        @current += 1 if @max == nil || @current < @max
        @message = message
        @block.call(self)
    end

    def decrement(message)
        raise 'Cannot change progress not currently running' unless @stage == :RUNNING
        @current -= 1 if @max == nil || @current > 0
        @message = message
        @block.call(self)
    end

    def current=(current)
        raise 'Cannot change progress not currently running' unless @stage == :RUNNING

        if @max != nil
            @current = @max if current > @max
            @current = 0 if current < 0
        end

        @current = current
    end

    def max=(max)
        if max != nil
            @max = 0 if max < 0
            @max = max

            @current = @max if current > @max
            @current = 0 if current < 0
            @current = current
        else
            @max = nil
        end
    end

    def message
        return @message if @message != nil
        @stage.id2name
    end

    def message=(message)
        raise 'Cannot change progress not currently running' unless @stage == :RUNNING

        @message = message

        @block.call(self)
    end

    def result
        raise 'Action is not finished yet!' unless @stage == :FINISHED
        @result
    end


    def begin(message)
        @message = message
        @stage = :RUNNING

        @block.call(self)
    end

    def complete(message, result)
        @stage = :FINISHED
        @result = result
        @current = @max
        @message = message

        @block.call(self)
    end

    def failed(message)
        @stage = :FAILED
        @current = 0
        @message = message

        @block.call(self)
    end
end