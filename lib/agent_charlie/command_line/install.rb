
require 'agent_charlie/action'
require 'trollop'

class AgentCharlie::CommandLine::Install < AgentCharlie::CommandLine::Base
    def initialize
        super("install", "Installs")
    end

    def execute(args)
        charlie = AgentCharlie::Charlie.instance

        subcommand_opts = Trollop::options(args) do
            banner <<-EOS.gsub(/^\t*/, '')
				The install command installs a specific tool.

				Usage:
				   #{$0} ... install [options] <tool>

				Options:
				EOS
            opt :reinstall, "Marks the specified tool for reinstallation", :short => '-r', :default => false
        end

        manifest = args.shift
        if ! manifest
            Trollop::die("You need to specify what to install")
        end

        if args.length != 0
            Trollop::die "Unknown additional arguments: #{args.inspect}"
        end

        AgentCharlie::CommandLine::Setup.tsort_setup(manifest, subcommand_opts)
    end
end

AgentCharlie::CommandLine.newCommand(AgentCharlie::CommandLine::Install)
