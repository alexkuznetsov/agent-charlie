require 'spec_helper'

describe 'jdk::ubuntu' do
    let(:title) { 'jvm_spec' }
    let(:facts) {{
        :config_user => 'bob',
        :config_group => 'bobgroup',
        :config_charlie_home => '/Users/yiyang/atlassian/agent-charlie',
        :charlie_repository => 'http://www.test-repository.example.com',
        :config_atlassian_home => 'some_target'
    }}


    context 'with ubuntu quantal' do
        let(:facts) {{
            :operatingsystem        => 'Ubuntu',
            :operatingsystemrelease => '12.10',
        }}
        it {
            # We're asserting two important things - the repository is correct,
            # and the package that it installs is the oracle-java7-installer
            should contain_exec('jdk_repo').with({
                'creates' => "/etc/apt/sources.list.d/webupd8team-java.list",
            })
            should contain_package('oracle-java7-installer').with({
                'ensure'  => 'installed',
            })
        }
    end

    context 'with ubuntu lucid' do
        let(:facts) {{
            :operatingsystem        => 'Ubuntu',
            :operatingsystemrelease => '11.04',
        }}
        it {
            # We're asserting two important things - the repository is correct,
            # and the package that it installs is the oracle-java7-installer
            should contain_exec('jdk_repo').with({
                'creates' => "/etc/apt/sources.list.d/webupd8team-java.list",
            })
            should contain_package('oracle-java7-installer').with({
                'ensure'  => 'installed',
            })
        }
    end

end
