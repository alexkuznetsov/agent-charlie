#!/bin/bash
# bootstrap.sh
# Return codes:
# - 3   - invalid arguments
# - 4   - charlie directories already exist
# - 5   - prerequisites are missing and could not be installed
# - 6   - failed to create agent charlie's files
# - 7   - failed to edit user .bashrc
# - 8   - unsupported platform
# - 9   - please drop sudo
# - 10  - a internal problem prevents agent charlie form installing - network connection, etc.
# - 11  - an external problem (something on your computer) prevents charlie from installing

function printMessage() {
    echo -ne "[\033[0;32mMSG\033[m] " 1>&2
    echo "$@" 1>&2
}

function printNotice() {
    echo -ne "[\033[0;35mHMM\033[m] " 1>&2
    echo "$@" 1>&2
}

function printError() {
    echo -ne "[\033[0;31mERR\033[m] " 1>&2
    echo "$@" 1>&2
}

function printUsage() {
    cat <<-EOF
		Usage: $0 [options] [command]

		Options

		    -target <dir>               The installation directory for several of Agent Charlie's manually
		                                installed binaries and environment scripts
		                                (default: ~/atlassian)
		    -agent_charlie_home <dir>   The installation directory for Agent Charlie's files
		                                (default: ~/atlassian/agent-charlie)
		    -tmp_directory <dir>        Directory to store temporary files in for Agent Charlie
		                                (default: ~/atlassian/agent-charlie/tmp)
		    -branch <branch>            Uses a different branch in Bitbucket (default: stable)
		    -overwrite                  Ignore checks and overwrite all files (default: false)

		Commands

		    help                        Shows this screen

		EOF
}

#======================================================================
# Arguments
#======================================================================
declare charlie_host="agent-charlie.syd.atlassian.com"
declare target="$HOME/atlassian"
declare agent_charlie_home="$target/agent-charlie"
declare tmp_directory="$agent_charlie_home/tmp"
declare branch="stable"

declare test_mode=false
declare overwrite=false
declare doupdate=false

# Process argrumensa
while [[ $# -ne 0 ]]; do
    case "$1" in
        -target)
            printMessage "Setting ATLASSIAN_HOME to '$2'"
            target="$2"
            shift 1
            ;;
        -agent_charlie_home)
            printMessage "Setting AGENT_CHARLIE_HOME to '$2'"
            agent_charlie_home="$2"
            shift 1
            ;;
        -tmp_directory)
            printMessage "Using temporary directory: '$2'"
            tmp_directory="$2"
            shift 1
            ;;
        -branch)
            printMessage "Using branch: '$2'"
            branch="$2"
            shift 1
            ;;
        -test)
            test_mode=true
            ;;
        -overwrite)
            overwrite=true
            ;;
        -update)
            doupdate=true
            ;;
        -*)
            printError "Unknown option: '$1'"
            printUsage
            exit 1
            ;;

        # Encounting normal arguments
        --)
            shift 1
            break
            ;;
        *)
            break
            ;;
    esac

    shift 1
done

#======================================================================
# Platform Check
#======================================================================
declare platform
declare -i release
if [ -f /usr/bin/lsb_release ]; then
    platform="`lsb_release -s -i`"
    release="`lsb_release -s -r | sed 's:\(\d*\)\.\(\d*\):\1\2:'`" # strip decimal point from release so we can treat it as an integer
else
    platform="`uname`"
fi
case "$platform" in
    Ubuntu)
        ;;
    Darwin)
        ;;
    *)
        echo "Platform not supported" 2>&1
        exit 8
        ;;
esac

#======================================================================
# Prerequisites
#======================================================================
declare -a prerequisites=(
    'curl'
    'tar'
    'ruby'
    'gem'
    'unzip'
    'finger'
)
declare curl_Ubuntu="curl"
declare tar_Ubuntu="tar"
declare ruby_Ubuntu="ruby"
declare gem_Ubuntu="rubygems"
declare unzip_Ubuntu="unzip"

declare -a Ubuntu_prerequisites=(
    # 'ruby-ffi'
    # 'libgnome-keyring-dev'
)

# rubygems package does not exist (and is not required) as of Ubuntu 14.04
if [[ "$platform" == "Ubuntu" && $release -lt 1404 ]]; then
        Ubuntu_prerequisites=("${Ubuntu_prerequisites[@]}" "rubygems") # Need this for the rubygems "gem"... seriously Vagrant
fi

#======================================================================
# Files
#======================================================================
declare charlie_tarball="https://bitbucket.org/atlassian/agent-charlie/get/${branch}.tar.gz"

#
# Charlie's Directories
#
declare -a charlie_directories=(
    "$tmp_directory"
    "$target/bin"
    "$target/env"
    "$agent_charlie_home"
    "$agent_charlie_home/setup"
    "$agent_charlie_home/metadata"
)

#
# Charlie's Files
#
declare -a charlie_files=(
    'config'
    'environment'
    'agent_charlie_environment'
)

# The following are populated by the initializeData() function
for file in "${charlie_files[@]}"; do
    declare "file_${file}"
done

declare file_config_path="$agent_charlie_home/setup/agent_bin/charlie.conf"
declare file_environment_path="$target/env/environment"
declare file_agent_charlie_environment_path="$target/env/agent-charlie-environment"

#
# Charlie's Angels
#
declare -a charlie_directory_log=()
declare -a charlie_file_log=()

#======================================================================
# Script starts here
#======================================================================
function bootstrap() {
    # entitlement checker
    if [[ `whoami` == "root" ]]
        then
        printNotice "Run this as ordinary user. Sudo will be done automatically"
        return 9
    fi

    while [[ $# -ne 0 ]]; do
        case "$1" in
            help)
                printUsage
                return 0
                ;;

            *)
                printError "What?: '$1'"
                printUsage
                return 3
                ;;
        esac

        shift 1
    done

    # Do this after we set directories
    initializeData

    # Problemo checksalsa
    # No problemo senorita
    $test_mode || {
        echo "$text_disclaimer" 1>&2
        printMessage "Press Enter to continue or Ctrl+C to abort..."
        read
    }

    # Taco grenadier
    if $doupdate; then
        systemUpdate || return 12
    fi
    ensureInstallable || return $? # 10/11
    $overwrite || checkCharlieInstallation || return 4
    installPrerequisites || return 5
    createCharlieFiles || return 6
    bashProfileConfig || return 7

    # Print messages
    # printMessage "Type the following to conclude setup:"

    # if [[ "$platform" == "Darwin" ]]; then
    #     printMessage ". ~/.profile"
    # elif [[ "$platform" == "Ubuntu" ]]; then
    #     printMessage ". ~/.bashrc"
    # fi
    # . "$target/env/environment"

    # We'll take the ssh-agent approach
    if [[ "$platform" == "Darwin" ]]; then
        echo "eval . ~/.profile"
    elif [[ "$platform" == "Ubuntu" ]]; then
        echo "eval . ~/.bashrc"
    fi

    return 0
}

function systemUpdate() {
    printMessage "Updating your system"

    case "$platform" in
        Ubuntu)
            sudo -p "Need your password to upgrade your packages: " apt-get -qq update 1>&2 || {
                printError "There was a problem running apt-get update"
                return 1
            }
            sudo -p "Need your password to upgrade your packages: " apt-get -qq upgrade 1>&2 || {
                printError "There was a problem upgrading your packages"
                return 1
            }
            ;;
        Darwin)
            sudo -p "Need your password to run a Software Update: " softwareupdate -i -a 1>&2 || {
                printError "There was a problem running Software Update"
                return 1
            }
            ;;
    esac
    return 0
}

function ensureInstallable() {
    printMessage "Making sure everything is O.K. before installation"

    #
    # External problems
    #

    # Check that we can access the internet
    ping -c 4 "www.bitbucket.com" &> /dev/null
    if [[ $? -ne 0 ]]; then
        printError "You are not connected to internet! Internet turn on!"
        return 11
    fi

    return 0
}

function checkCharlieInstallation() {
    printMessage 'Checking for previous installation of Agent Charlie'

    if [[ -e "$target" ]]; then
        printError "Target directory '$target' already exists!"
        return 1
    fi

    if [[ -e "$agent_charlie_home" ]]; then
        printError "Agent Charlie home directory '$agent_charlie_home' already exists!"
        return 1
    fi

    local directory
    for directory in "${charlie_directories[@]}"; do
        if [[ -e "$directory" ]]; then
            printError "Directory '$directory' already exists!"
            return 1
        fi
    done

    return 0
}

function installPrerequisites() {
    printMessage 'Checking for prerequisites'

    declare -a missingPackages

    #
    # Installing Packages
    #
    local prereq
    for prereq in "${prerequisites[@]}"; do
        if ! type "$prereq" &> /dev/null; then
            local package="${prereq}_${platform}"
            missingPackages+=("${!package-${prereq}}")
        fi
    done

    case "$platform" in
        Ubuntu)
            for prereq in "${Ubuntu_prerequisites[@]}"; do
                if ! dpkg -s "$prereq" &> /dev/null; then
                    missingPackages+=("$prereq")
                fi
            done

            if [[ "${#missingPackages[@]}" -ne 0 ]]; then
                printNotice "Missing packages: ${missingPackages[*]}"
                sudo -p "We need your password to install these packages: " apt-get -qq install "${missingPackages[@]}" 1>&2 || {
                    printError "Error while trying to install prerequisites:" "${missingPackages[@]}"
                    return 1
                }
            fi
            ;;
        Darwin)
            if [[ "${#missingPackages[@]}" -ne 0 ]]; then
                printError "You should not be seeing this message. Are you running OS X 10.7/10.8?"
                return 1
            fi
            ;;
    esac

    #
    # Install Ruby Gems
    #
    if ! type bundle &> /dev/null; then
        sudo -p "We need your password to install Bundler: " gem install bundler --quiet 1>&2 || return 1
    fi

    return 0
}

function createCharlieFiles() {
    printMessage 'Creating Agent Charlie directories and files'

    local directory
    for directory in "${charlie_directories[@]}"; do
        mkdir -p "$directory"
        charlie_directory_log+=("$directory")
    done

    # Download Agent Charlie
    if $test_mode; then
        [[ -f testing.zip ]] || {
            printError "Test Mode activated but 'testing.zip' not found!"
            return 1
        }

        unzip -qq -o testing.zip -d "$agent_charlie_home/setup" || {
            printError "Could not unzip 'testing.zip'!"
            return 1
        }
    else
        charlie_file_log+=("$tmp_directory/master.tar.gz")
        (
            curl -s -L -o "$tmp_directory/master.tar.gz" "$charlie_tarball" &&
            tar -C "$agent_charlie_home/setup" -xzf "$tmp_directory/master.tar.gz" --strip-components 1
        ) || {
            printError 'Could not download/extract Agent Charlie!'
            return 1
        }
    fi

    # Create the rest of the files
    local file
    for file in "${charlie_files[@]}"; do
        local fileText="file_${file}"
        local installPath="file_${file}_path"

        echo "${!fileText}" > "${!installPath}"
        charlie_file_log+=("${!installPath}")
    done

    # Symlink charlie into our binary directory
    [[ -L "$agent_charlie_home/bin" ]] && rm -f "$agent_charlie_home/bin"
    ln -fs "$agent_charlie_home/setup/agent_bin" "$agent_charlie_home/bin" || {
        printError "There was a problem symlinking '$agent_charlie_home/setup/agent_bin' to '$agent_charlie_home/bin'"
        return 1
    }

    # Run some necessary commands
    cd "$agent_charlie_home/setup" &> /dev/null && {
        bundle install --deployment --without test:development --path .bundle --binstubs bin --quiet || {
            printError "Bundler failed to install Agent Charlie dependencies"
            return 1
        }
        cd - &> /dev/null
    }

    return 0
}

function bashProfileConfig() {

    # Note: On Ubuntu, .bashrc is not executed in a non-interactive non-login shell (there is a return)
    # Whereas on OS X, .bashrc is not executed at all.

    # So with this configuration there's a chance of running the environment twice (which is safe)

    printMessage 'Configuring your ~/.bash_profile'
    if ! egrep -e "\$HOME/\.profile" "$HOME/.bashrc" &> /dev/null; then
        cat <<-EOF >> "$HOME/.bash_profile"
        
			. "$HOME/.profile"

			EOF
    else
        printNotice "Noticed .bash_profile already referenced .profile. No changes was made."
    fi

    printMessage 'Configuring your ~/.profile'

    if ! egrep -e "^#=== Agent Charlie Environment ===$" "$HOME/.profile" &> /dev/null; then
        cat <<-EOF >> "$HOME/.profile"

			#=== Agent Charlie Environment ===
			. "$target/env/environment"

			EOF
    else
        printNotice "Noticed that your ~/.profile already had Agent Charlie config. Skipping..."
        printNotice "Be warned that this configuration may be wrong if you moved Agent Charlie directories."
    fi

    printMessage 'Configuring your ~/.bashrc'

    if ! egrep -e "^#=== Agent Charlie Environment ===$" "$HOME/.bashrc" &> /dev/null; then
        cat <<-EOF >> "$HOME/.bashrc"

			#=== Agent Charlie Environment ===
			. "$target/env/environment"

			EOF
    else
        printNotice "Noticed that your ~/.bashrc already had Agent Charlie config. Skipping..."
        printNotice "Be warned that this configuration may be wrong if you moved Agent Charlie directories."
    fi
}

function initializeData() {
    printMessage 'Initializing data'

    text_disclaimer=$(cat <<-EOF
		=========================================================
		 Agent Charlie Bootstrap Script
		---------------------------------------------------------
		1. What does this script do?

		   This script will perform the following operations:
		   - Ensures curl, tar, ruby, gem, and unzip commands
		     exist on your computer. These should be pre-installed
		     on OSX. We use apt-get to install these on Ubuntu.
		   - Installs the "Bundler" gem for Ruby
		     (gem install bundler)
		   - Creates the following directories:
		       $target
		       $agent_charlie_home
		       $tmp_directory
		   - Adds a few lines to your ~/.bashrc

		2. Will bootstrapping damage my computer?

		   Not unless you consider the programs listed above to
		   be dangerous.

		   Until you run agent-charlie, bootstrapping is completely
		   reversable:

		      rm -rf ~/atlassian/agent-charlie
		      vim .bashrc  # Delete it yourself
		      gem uninstall bundler
		      apt-get remove curl tar ruby gem unzip

		For more information, please see:
		  https://extranet.atlassian.com/x/Ao1Vfg

		EOF
    )

    file_config=$(cat <<-EOF
		user : '$(whoami)'
		group : '$(id -g -n)'
		home : '$HOME'
		atlassian_home : '$target'
		charlie_home : '$agent_charlie_home'
		tmp_directory : '$tmp_directory'
		EOF
    )

    file_environment=$(cat <<-EOF
		#!/bin/bash

		export ATLASSIAN_HOME="$target"

		# Add atlassian binaries
		if echo ":\$PATH:" | grep -Eqve "^(\\\\.|[^\\\\])*:\$ATLASSIAN_HOME/bin:"; then
		    export PATH="\$ATLASSIAN_HOME/bin:\$PATH"
		fi

		# Sources all *-environment files in the current folder
		for envfile in \`ls "\$ATLASSIAN_HOME/env/"*"-environment" 2>/dev/null\`; do
		    . "\$envfile"
		done
		EOF
    )

    file_agent_charlie_environment=$(cat <<-EOF
		export AGENT_HOME="$agent_charlie_home"
        export AGENT_TMP="$tmp_directory"

		if echo ":\$PATH:" | grep -Eqve "^(\\\\.|[^\\\\])*:\$AGENT_HOME/bin:"; then
		    export PATH="\$AGENT_HOME/bin:\$PATH"
		fi

		EOF
    )
}

# BEGIN
bootstrap "$@"
bootstrapExitStatus=$?

if [[ $bootstrapExitStatus -ne 0 ]]; then
    # Run some cleanup
    printError "Oh dear! Something bad happened. Here's the files and directories that we created:"
    printError "Files:"
    for file in "${charlie_file_log[@]}"; do
        printError " - $file"
    done

    printError "Directories:"
    for directory in "${charlie_directory_log[@]}"; do
        printError " - $directory"
    done
else
    printMessage "Successfully installed Agent Charlie!"
fi

exit $bootstrapExitStatus
